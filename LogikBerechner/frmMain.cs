﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace LogikBerechner
{
    public partial class frmMain : Form
    {
        private abstract class Expression
        {
            public abstract bool GetValue(Dictionary<char, bool> variableValues);
            public abstract void AddToList(List<Expression> lst);
            public abstract void AddUsedVariables(List<char> list);
            public abstract void DetectRecursion(char startVar);
        }
        private class Constant : Expression
        {
            public bool value;
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                return value;
            }
            public override bool Equals(object obj)
            {
                return obj is Constant && ((Constant)obj).value == value;
            }
            public override int GetHashCode() { return 0; }
            public override string ToString()
            {
                return value ? "1" : "0";
            }
            public override void AddToList(List<Expression> lst) { }
            public override void AddUsedVariables(List<char> list) { }
            public override void DetectRecursion(char startVar) { }
        }
        private class Variable : Expression
        {
            public char name;
            public static Dictionary<char, Expression> complexVariables = new Dictionary<char, Expression>();
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                if (complexVariables.ContainsKey(name)) return complexVariables[name].GetValue(variableValues);
                else return variableValues[name];
            }
            public override string ToString() { return name.ToString(); }
            public override bool Equals(object obj)
            {
                if (!(obj is Variable)) return false;
                return ((Variable)obj).name.ToString().ToLower() == name.ToString().ToLower();
            }
            public override int GetHashCode() { return 0; }
            public override void AddToList(List<Expression> lst) 
            {
                if (complexVariables.ContainsKey(name)) complexVariables[name].AddToList(lst);
            }
            public override void AddUsedVariables(List<char> list)
            {
                if (list.Contains(name)) return;
                if (complexVariables.ContainsKey(name)) complexVariables[name].AddUsedVariables(list);
                else list.Add(name);
            }
            public override void DetectRecursion(char startVar)
            {
                if (name == startVar) throw new InvalidOperationException("Variablen dürfen nicht rekursiv definiert werden!");
                else if (complexVariables.ContainsKey(name)) complexVariables[name].DetectRecursion(startVar);
            }
        }
        private class Not : Expression
        {
            public Expression expression;
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                return !expression.GetValue(variableValues);
            }
            public override string ToString() 
            {
                if (expression is DoublesidedExpression || expression is Satisfiable) return "¬(" + expression.ToString() + ")"; 
                else return "¬" + expression.ToString(); 
            }
            public override bool Equals(object obj)
            {
                return obj is Not && ((Not)obj).expression.Equals(expression);
            }
            public override int GetHashCode() { return 0; }
            public override void AddToList(List<Expression> lst) 
            {
                expression.AddToList(lst);
                if (!lst.Contains(this)) lst.Add(this);
            }
            public override void AddUsedVariables(List<char> list)
            {
                expression.AddUsedVariables(list);
            }
            public override void DetectRecursion(char startVar)
            {
                expression.DetectRecursion(startVar);
            }
        }
        private abstract class DoublesidedExpression : Expression
        {
            public Expression left, right;
            protected abstract char GetOperatorChar();
            protected virtual bool IsAssociativeTo(DoublesidedExpression expression) { return false; }
            public override string ToString()
            {
                return
                    (left is DoublesidedExpression && !IsAssociativeTo((DoublesidedExpression)left) ? "(" + left.ToString() + ")" : left.ToString())
                    + " " + GetOperatorChar().ToString() + " " +
                    (right is DoublesidedExpression && !IsAssociativeTo((DoublesidedExpression)right) ? "(" + right.ToString() + ")" : right.ToString());
            }
            public override bool Equals(object obj)
            {
                return obj is DoublesidedExpression && ((((DoublesidedExpression)obj).left.Equals(left) && ((DoublesidedExpression)obj).right.Equals(right)) || (((DoublesidedExpression)obj).left.Equals(right) && ((DoublesidedExpression)obj).right.Equals(left)));
            }
            public override int GetHashCode() { return 0; }
            public override void AddToList(List<Expression> lst) 
            {
                left.AddToList(lst);
                right.AddToList(lst);
                if (!lst.Contains(this)) lst.Add(this);
            }
            public override void AddUsedVariables(List<char> list)
            {
                left.AddUsedVariables(list);
                right.AddUsedVariables(list);
            }
            public override void DetectRecursion(char startVar)
            {
                left.DetectRecursion(startVar);
                right.DetectRecursion(startVar);
            }
        }
        private class Or : DoublesidedExpression
        {            
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                return left.GetValue(variableValues) || right.GetValue(variableValues);                
            }
            protected override char GetOperatorChar() { return '∨'; }
            public override bool Equals(object obj)
            {
                return obj is Or && base.Equals(obj);
            }
            public override int GetHashCode() { return 0; }
            protected override bool IsAssociativeTo(DoublesidedExpression expression)
            {
                return expression is Or;
            }
        }
        private class And : DoublesidedExpression
        {
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                return left.GetValue(variableValues) && right.GetValue(variableValues);
            }
            protected override char GetOperatorChar() { return '∧'; }
            public override bool Equals(object obj)
            {
                return obj is And && base.Equals(obj);
            }
            public override int GetHashCode() { return 0; }
            protected override bool IsAssociativeTo(DoublesidedExpression expression)
            {
                return expression is And;
            }
        }
        private class Implication : DoublesidedExpression
        {
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                return !left.GetValue(variableValues) || right.GetValue(variableValues);
            }
            protected override char GetOperatorChar() { return '⇒'; }
            public override bool Equals(object obj)
            {
                return obj is Implication && left.Equals(((Implication)obj).left) && right.Equals(((Implication)obj).right);
            }
            public override int GetHashCode() { return 0; }
        }
        private class Equivalent : DoublesidedExpression
        {
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                return left.GetValue(variableValues) == right.GetValue(variableValues);
            }
            protected override char GetOperatorChar() { return '⇔'; }
            public override bool Equals(object obj)
            {
                return obj is Equivalent && base.Equals(obj);
            }
            public override int GetHashCode() { return 0; }
        }
        private class ExclusiveOr : DoublesidedExpression
        {
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                return left.GetValue(variableValues) ^ right.GetValue(variableValues);
            }
            protected override char GetOperatorChar() { return '⊕'; }
            public override bool Equals(object obj)
            {
                return obj is ExclusiveOr && base.Equals(obj);
            }
            public override int GetHashCode() { return 0; }
            protected override bool IsAssociativeTo(DoublesidedExpression expression)
            {
                return expression is ExclusiveOr;
            }
        }
        private class Nand : DoublesidedExpression
        {
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                return !(left.GetValue(variableValues) && right.GetValue(variableValues));
            }
            protected override char GetOperatorChar() { return '↑'; }
            public override bool Equals(object obj)
            {
                return obj is Nand && base.Equals(obj);
            }
            public override int GetHashCode() { return 0; }
        }
        private class Satisfies : DoublesidedExpression
        {
            private bool hasValue, value;
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                if (!hasValue) CalculateValue();
                return value;
            }
            private void CalculateValue()
            {
                hasValue = true;
                List<char> variables = new List<char>();
                left.AddUsedVariables(variables);
                right.AddUsedVariables(variables);
                List<Dictionary<char, bool>> combinations = new List<Dictionary<char,bool>>();
                Dictionary<char, bool> variableValues = new Dictionary<char, bool>();
                foreach (char ch in variables) variableValues.Add(ch, false);
                GetVariableCombinations(variables, 0, combinations, variableValues);
                foreach (Dictionary<char, bool> combination in combinations)
                {
                    if (left.GetValue(combination) && !right.GetValue(combination)) { value = false; return; }
                }
                value = true;
            }
            protected override char GetOperatorChar() { return '⊧'; }
            public override int GetHashCode() { return 0; }
            public override bool Equals(object obj)
            {
                return obj is Satisfies && left.Equals(((Satisfies)obj).left) && right.Equals(((Satisfies)obj).right);
            }
            public override void AddToList(List<Expression> lst)
            {
                if (!lst.Contains(this)) lst.Add(this); // No childs, since they have irrelevant values
            }
            public override void AddUsedVariables(List<char> list) { }
        }
        private class Satisfiable : Expression
        {
            private bool hasValue, value;
            public Expression expression;
            public override bool GetValue(Dictionary<char, bool> variableValues)
            {
                if (!hasValue) CalculateValue();
                return value;
            }
            private void CalculateValue()
            {
                hasValue = true;
                List<char> variables = new List<char>();
                expression.AddUsedVariables(variables);
                List<Dictionary<char, bool>> combinations = new List<Dictionary<char, bool>>();
                Dictionary<char, bool> variableValues = new Dictionary<char, bool>();
                foreach (char ch in variables) variableValues.Add(ch, false);
                GetVariableCombinations(variables, 0, combinations, variableValues);
                foreach (Dictionary<char, bool> combination in combinations)
                {
                    if (expression.GetValue(combination)) { value = true; return; }
                }
                value = false;
            }
            public override bool Equals(object obj)
            {
                return obj is Satisfiable && expression.Equals(((Satisfiable)obj).expression);
            }
            public override void AddToList(List<Expression> lst)
            {
                if (!lst.Contains(this)) lst.Add(this); // No childs, since they have irrelevant values
            }
            public override int GetHashCode() { return 0; }
            public override void AddUsedVariables(List<char> list) { }
            public override string ToString()
            {
                return "⊧ " + expression.ToString();
            }
            public override void DetectRecursion(char startVar)
            {
                expression.DetectRecursion(startVar);
            }
        }

        public frmMain()
        {
            InitializeComponent();
            txt.SelectionStart = txt.TextLength;
        }

        private void cmdCalculate_Click(object sender, EventArgs e)
        {
            
            List<Expression> conditions = new List<Expression>();
            Variable.complexVariables.Clear();
            try
            {
                string data = FormatExpressions(txt.Text);
                foreach (string line in data.Split('\r')) 
                {
                    if (line.Length == 0 || line.StartsWith("#")) continue;
                    Expression expression = ParseExpression(line);
                    if (expression != null) conditions.Add(expression);                     
                }
                foreach (KeyValuePair<char, Expression> complexVar in Variable.complexVariables) complexVar.Value.DetectRecursion(complexVar.Key);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Parser-Fehler: " + ex.Message);
                return;
            }

            if (conditions.Count == 0) { MessageBox.Show("Keine Bedingungen gefunden!"); return; }

            List<char> variables = new List<char>();
            foreach (Expression condition in conditions) condition.AddUsedVariables(variables);            
            variables.Sort();

            Dictionary<char, bool> variableValues = new Dictionary<char,bool>();
            foreach (char ch in variables) variableValues.Add(ch, false);

            List<Dictionary<char, bool>> solutions = new List<Dictionary<char,bool>>();
            FindSolutions(variables, 0, solutions, variableValues, conditions);

            string msg;
            if (solutions.Count == 0) msg = "Die eingegebenen Bedingungen sind unerfüllbar!\r\n";
            else if (solutions.Count == Math.Pow(2, variables.Count)) msg = "Die eingegebenen Bedingungen sind eine Tautologie!\r\n";
            else
            {
                msg = "Die folgenden Variablen-Werte erfüllen die angegebenen Bedingungen:\r\n\r\n";
                foreach (char varName in variables) msg += varName.ToString() + "\t";
                foreach (Dictionary<char, bool> solution in solutions)
                {
                    msg += "\r\n";
                    foreach (KeyValuePair<char, bool> current in solution) msg += (current.Value ? "1" : "0") + "\t";
                }
            }

            msg += "\r\n\r\nEs wurden folgende Bedingungen erkannt:";
            foreach (Expression condition in conditions) msg += "\r\n" + condition.ToString();

            if (Variable.complexVariables.Count > 0)
            {
                msg += "\r\n\r\nwobei:";
                foreach (KeyValuePair<char, Expression> complexVar in Variable.complexVariables) msg += "\r\n" + complexVar.Key + " := " + complexVar.Value.ToString();
            }

            MessageBox.Show(msg);
        }

        private void cmdExportCSV_Click(object sender, EventArgs e)
        {            
            List<Expression> conditions = new List<Expression>();
            Variable.complexVariables.Clear();
            try
            {
                string data = FormatExpressions(txt.Text);
                foreach (string line in data.Split('\r'))
                {
                    if (line.Length == 0 || line.StartsWith("#")) continue;
                    Expression expression = ParseExpression(line);
                    if (expression != null) conditions.Add(expression);
                }
                foreach (KeyValuePair<char, Expression> complexVar in Variable.complexVariables) complexVar.Value.DetectRecursion(complexVar.Key);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Parser-Fehler: " + ex.Message);
                return;
            }

            if (conditions.Count == 0) return;            

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.AddExtension = true;
            dlg.CheckPathExists = true;
            dlg.DefaultExt = ".csv";
            dlg.Filter = "Comma Seperated Values|.csv";
            dlg.OverwritePrompt = true;
            dlg.Title = "Wertetabelle exportieren";

            if (dlg.ShowDialog() != DialogResult.OK) { MessageBox.Show("Keine Bedingungen gefunden!"); return; }

            List<char> variables = new List<char>();
            foreach (Expression condition in conditions) condition.AddUsedVariables(variables);            
            variables.Sort();

            string output = "";
            foreach (char variable in variables) output += variable + "\t";
            output += "\t";

            List<Expression> expressions = new List<Expression>();
            foreach (Expression condition in conditions) condition.AddToList(expressions);
            foreach (Expression expression in expressions) output += expression.ToString() + "\t";
            output += "Mögliche Lösung\r\n";


            List<Dictionary<char, bool>> combinations = new List<Dictionary<char, bool>>();
            Dictionary<char, bool> variableValues = new Dictionary<char, bool>();
            foreach (char ch in variables) variableValues.Add(ch, false);
            GetVariableCombinations(variables, 0, combinations, variableValues);

            foreach (Dictionary<char, bool> combination in combinations)
            {
                foreach (char variable in variables) output += (combination[variable] ? "1" : "0") + "\t";
                output += "\t";

                foreach (Expression expression in expressions) output += (expression.GetValue(combination) ? "1" : "0") + "\t";
                output += IsSolution(combination, conditions) ? "X\r\n" : "\r\n";
            }

            File.Delete(dlg.FileName);
            using (StreamWriter writer = new StreamWriter(dlg.FileName, false, System.Text.Encoding.Unicode))
            {
                writer.Write(output);
                writer.Close();
            }
            Process.Start(dlg.FileName);
        }

        private static string FormatExpressions(string data)
        {
            return data.Replace("\r\n", "\r").Replace(" ", "").Replace("\t", "").ToUpper()
                .Replace("||", "|").Replace('+', '|').Replace(',', '|')
                .Replace('!', '-').Replace("&&", "&")
                .Replace("<=>", "=").Replace("=>", ">")
                .Replace("!=", "*").Replace("|==", "$").Replace("|=", "%")
                .Replace("!&", "@").Replace("-&", "@").Replace("^|", "@")
                .Replace(":=", ":");
        }

        private static void GetVariableCombinations(List<char> variables, int varIndex, List<Dictionary<char, bool>> combinations, Dictionary<char, bool> variableValues)
        {
            if (varIndex < variables.Count)
            {
                char curVar = variables[varIndex];

                variableValues[curVar] = false;
                GetVariableCombinations(variables, varIndex + 1, combinations, variableValues);

                variableValues[curVar] = true;
                GetVariableCombinations(variables, varIndex + 1, combinations, variableValues);
            }
            else combinations.Add(CloneSolution(variableValues));
        }

        private static void FindSolutions(List<char> variables, int varIndex, List<Dictionary<char, bool>> solutions, Dictionary<char, bool> variableValues, List<Expression> conditions)
        {
            if (varIndex < variables.Count)
            {
                char curVar = variables[varIndex];

                variableValues[curVar] = false;
                FindSolutions(variables, varIndex + 1, solutions, variableValues, conditions);

                variableValues[curVar] = true;
                FindSolutions(variables, varIndex + 1, solutions, variableValues, conditions);
            }
            else if (IsSolution(variableValues, conditions)) solutions.Add(CloneSolution(variableValues));
        }
        private static bool IsSolution(Dictionary<char, bool> variableValues, List<Expression> conditions)
        {
            foreach (Expression condition in conditions)
            {
                if (!condition.GetValue(variableValues)) return false;
            }
            return true;
        }
        private static Dictionary<char, bool> CloneSolution(Dictionary<char, bool> solution)
        {
            Dictionary<char, bool> result = new Dictionary<char, bool>();
            foreach (KeyValuePair<char, bool> act in solution) result.Add(act.Key, act.Value);
            return result;
        }     

        private static Expression ParseExpression(string data) 
        {
            if (data.Contains(":"))
            {
                string[] split = data.Split(':');
                if (split.Length != 2 || split[0].Length != 1) throw new InvalidOperationException("Ungültige Verwendung des := Operators!");
                Variable.complexVariables.Add(split[0][0], ParseExpression(split[1]));
                return null;
            }
            else
            {
                Queue<char> chars = new Queue<char>();
                foreach (char ch in data) chars.Enqueue(ch);
                return ParseExpression(chars);
            }
        }
        private static Expression ParseExpression(Queue<char> chars)
        {
            List<object> tokens = new List<object>();

            while (chars.Count > 0)
            {
                char curChar = chars.Dequeue();
                if (curChar == '(')
                {
                    int nestedBrackets = 0;
                    Queue<char> bracketContent = new Queue<char>();
                    while (true)
                    {
                        char innerChar = chars.Dequeue();
                        if (innerChar == '(') { nestedBrackets++; bracketContent.Enqueue(innerChar); }
                        else if (innerChar == ')')
                        {
                            if (nestedBrackets > 0) { nestedBrackets--; bracketContent.Enqueue(innerChar); }
                            else
                            {
                                tokens.Add(ParseExpression(bracketContent));
                                break;
                            }
                        }
                        else bracketContent.Enqueue(innerChar);
                    }
                }
                else if (curChar == '=' || curChar == '>' || curChar == '|' || curChar == '&' || curChar == '-' || curChar == '*' || curChar == '@' || curChar == '$' || curChar == '%') tokens.Add(curChar);
                else if (curChar == '0') tokens.Add(new Constant() { value = false });
                else if (curChar == '1') tokens.Add(new Constant() { value = true });
                else if ((curChar >= 'A' && curChar <= 'Z')) tokens.Add(new Variable() { name = curChar });
                else throw new InvalidOperationException("Unerwartetes Zeichen: " + curChar + "!");
            }

            return ParseExpression(tokens);
        }
        private static Expression ParseExpression(List<object> tokens)
        {
            if (tokens.Count == 0) throw new InvalidOperationException("Ungültige Formel!");

            if (tokens[0] is char && ((char)tokens[0]) == '%') // % is used internally for |=
            {
                tokens.RemoveAt(0);
                if (tokens.Count == 0) throw new InvalidOperationException("Ungültige Verwendung des |= Operators!");
                return new Satisfiable() { expression = ParseExpression(tokens) };
            }

            int index = tokens.IndexOf('$'); // $ is used internally for |==
            if (index != -1) return SplitInto(tokens, index, new Satisfies()); 
            
            index = tokens.IndexOf('=');
            if (index != -1) return SplitInto(tokens, index, new Equivalent());
            
            index = tokens.IndexOf('>');
            if (index != -1) return SplitInto(tokens, index, new Implication());

            index = tokens.IndexOf('*');
            if (index != -1) return SplitInto(tokens, index, new ExclusiveOr());

            index = tokens.IndexOf('@'); // @ is used internally for NAND
            if (index != -1) return SplitInto(tokens, index, new Nand());
             
            index = tokens.IndexOf('|');
            if (index != -1) return SplitInto(tokens, index, new Or()); 

            index = tokens.IndexOf('&');
            if (index != -1) return SplitInto(tokens, index, new And());

            if (tokens[0] is char && ((char)tokens[0]) == '-')
            {
                // unary operators
                tokens.RemoveAt(0);
                if (tokens.Count == 0) throw new InvalidOperationException("Ungültige Verwendung des Negations-Operators!");
                return new Not() { expression = ParseExpression(tokens) };
            }
            
            if (tokens.Count == 1 && tokens[0] is Expression) return ((Expression)tokens[0]);

            throw new InvalidOperationException("Ungültige Formel!");
        }
        private static DoublesidedExpression SplitInto(List<object> tokens, int index, DoublesidedExpression dest)
        {
            List<object> left = new List<object>();
            List<object> right = new List<object>();
            for (int i = 0; i < index; i++) left.Add(tokens[i]);
            for (int i = index + 1; i < tokens.Count; i++) right.Add(tokens[i]);
            dest.left = ParseExpression(left);
            dest.right = ParseExpression(right);
            return dest;
        }
    }
}